# Deploy BE Docker

This is the Docker containing a BE instance for the FE to be able to work autonomously.

## 1.DATA

#### IMPORT && Updatade DATA: 
`mongodump --host=dev.mongo.insylo.io  --port=27017 --db=smartSiloAPI`


#### In /dump initial, Data to be dumped:
`mongorestore --host=localhost  --port=27017 --db=smartSiloAPI ./dump/smartSiloAPI`

#### DB Location:

`/usr/local/var/mongodb`


## 2.MONGODB

#### START && STOP service:
`brew services start mongodb-community@3.6`

`brew services stop mongodb-community@3.6`


## 3.DOCKER
#### RUN
`docker-compose up`
#### STOP
`docker-compose down`



#### INFO 
In /data:

    - id-private-api key file
    - quotes.json



